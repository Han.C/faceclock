package com.daelibs.facelogin;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import com.daelibs.facelogin.data.DatabaseRepository;
import com.daelibs.facelogin.data.Employees;
import com.daelibs.facelogin.data.Event;
import com.daelibs.facelogin.data.employeephoto;
import com.daelibs.facelogin.webservices.UpdateClient;
import com.daelibs.facelogin.webservices.UpdateClientException;
import com.daelibs.facelogin.webservices.Util;
import com.daelibs.facelogin.webservices.getinfoclient;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class LoginActivity extends Activity {
    private boolean photoSaved = false;
    private File previousImageFile;
    private File previousResizedImageFile;
    private File imageFile;
    private File resizedImageFile;
    private CountDownTimer timer;
    public String loggerSerial;
    public ImageView ivPhoto;
    Button nextbutton;
    Button clearbutton;
    Button updatebutton;
    public int userserverid;
    public int currenteventid;
    Button proceedbutton;
    public Handler handler2;
    public Runnable runnableCode;
    private byte[] imageBinary;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginactivity);
        updatebutton = (Button) findViewById(R.id.updatebutton);
        nextbutton = (Button) findViewById(R.id.nextbutton);
        proceedbutton = (Button) findViewById(R.id.proceedbutton);
        clearbutton = (Button) findViewById(R.id.clearbutton);
        ivPhoto = (ImageView) findViewById(R.id.iv_photo);
        loggerSerial = Util.getSerial(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        handler2 = new Handler();

        runnableCode = new Runnable() {
            @Override
            public void run() {
                upload();
                handler2.postDelayed(this, 120000);
            }
        };

        handler2.post(runnableCode);



        connect();


        nextbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connect();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        EditText usernametext = (EditText) findViewById(R.id.username);
                        EditText password = (EditText) findViewById(R.id.password);
                        String username = usernametext.getText().toString().trim();
                        if (usernametext.getText().toString().trim().length() > 0) {
                            if (password.getText().toString().trim().length() > 0) {
                                Employees user = login(usernametext.getText().toString(), password.getText().toString());

                                if(user==null) {

                                    Toast.makeText(getApplicationContext(), "Incorrect Username or Password", Toast.LENGTH_LONG).show();
                                    usernametext.setText("");
                                    password.setText("");
                                }
                                else{
                                    userserverid = (int) user.getServerId();
                                    takePhoto();
                                }
                            }else{
                                Toast.makeText(getApplicationContext(), "Please enter username and password", Toast.LENGTH_LONG).show();
                            }
                        }else {
                            Toast.makeText(getApplicationContext(), "Please enter username and password", Toast.LENGTH_LONG).show();
                            }
                    }
                }, 500);

            }

        });



        proceedbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText usernametext = (EditText) findViewById(R.id.username);
                EditText password = (EditText) findViewById(R.id.password);
                Employees user = login(usernametext.getText().toString(), password.getText().toString());

                if(user==null){
                    Toast.makeText(getApplicationContext(), "Incorrect Username or Password", Toast.LENGTH_LONG).show();
                    usernametext.setText("");
                    password.setText("");
                }else{
                    userserverid = (int) user.getServerId();

                    String firstname = user.getFirstName();
                    String lastname = user.getLastName();
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setMessage(firstname + " " + lastname).setPositiveButton("Clock In", dialogClickListener)
                            .setNegativeButton("Clock Out", dialogClickListener).show();
                }
            }
        });




        updatebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(LoginActivity.this, "Checking for updates...", Toast.LENGTH_LONG).show();
                onClickCheckForUpdates();
            }
        });


        clearbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText usernametext = (EditText) findViewById(R.id.username);
                EditText password = (EditText) findViewById(R.id.password);
                usernametext.setText("");
                password.setText("");
                ivPhoto.setVisibility(View.INVISIBLE);
                ivPhoto.setImageDrawable(null);
                nextbutton.setVisibility(View.VISIBLE);
                proceedbutton.setVisibility(View.INVISIBLE);
            }
        });

    }



    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    EditText usernametext = (EditText) findViewById(R.id.username);
                    EditText password = (EditText) findViewById(R.id.password);
                    DatabaseRepository repo = new DatabaseRepository(LoginActivity.this);
                    Date now = new Date();
                    Event event = new Event();
                    event.setClockInOut(1);
                    event.setEmployeeId(userserverid);
                    event.setEventTime(now);
                    int id = (int)repo.insertevents(event);
                    currenteventid = id;
                    employeephoto EmployeePhoto = new employeephoto();
                    EmployeePhoto.setEventId(id);

                    EmployeePhoto.setEmployeeId(userserverid);
                    EmployeePhoto.setFileName(resizedImageFile.getAbsolutePath());
                    EmployeePhoto.setSent(0);
                    repo.insertEmployeePhoto(EmployeePhoto);
                    ivPhoto.setVisibility(View.INVISIBLE);
                    nextbutton.setVisibility(View.VISIBLE);
                    proceedbutton.setVisibility(View.INVISIBLE);
                    usernametext.setText("");
                    password.setText("");
                    upload();

                    break;



                case DialogInterface.BUTTON_NEGATIVE:

                    usernametext = (EditText) findViewById(R.id.username);
                    password = (EditText) findViewById(R.id.password);
                    repo = new DatabaseRepository(LoginActivity.this);
                    now = new Date();
                    Event event2 = new Event();
                    event2.setClockInOut(2);
                    event2.setEmployeeId(userserverid);
                    event2.setEventTime(now);
                    int i2 = (int)repo.insertevents(event2);
                    currenteventid = i2;
                    employeephoto EmployeePhoto2 = new employeephoto();
                    EmployeePhoto2.setEventId(i2);

                    EmployeePhoto2.setEmployeeId(userserverid);
                    EmployeePhoto2.setFileName(resizedImageFile.getAbsolutePath());
                    EmployeePhoto2.setSent(0);
                    repo.insertEmployeePhoto(EmployeePhoto2);
                    upload();
                    ivPhoto.setVisibility(View.INVISIBLE);
                    nextbutton.setVisibility(View.VISIBLE);
                    proceedbutton.setVisibility(View.INVISIBLE);
                    usernametext.setText("");
                    password.setText("");
                    break;
            }
        }
    };
    private void takePhoto() {


            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
            previousImageFile = imageFile;
            previousResizedImageFile = resizedImageFile;


            try {
                imageFile = File.createTempFile("incident-", ".jpg", getPhotoDir());
            } catch (IOException e) {
                Toast.makeText(this, "Error Creating Photo File", Toast.LENGTH_LONG).show();
                return;
            }


            Uri outputFileUri = Uri.fromFile(imageFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            startActivityForResult(intent, 99);
        }


    public File getPhotoDir() {
        File f = new File(this.getExternalFilesDir(null), "IPhoto");
        if (!f.isDirectory()) {
            f.mkdir();
        }
        return f;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 99) {
            switch (resultCode) {
                case Activity.RESULT_CANCELED: {
                    break;
                }
                case Activity.RESULT_OK: {

                        if (previousImageFile != null && previousImageFile.exists()) {
                            previousImageFile.delete();
                        }

                    photoSaved = true;

                    String path = imageFile.getAbsolutePath();

                    BitmapFactory.Options opt = new BitmapFactory.Options();
                    opt.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(path, opt);

                    int targetW = 640;
                    int targetH = 480;

                    int photoW = opt.outWidth;
                    int photoH = opt.outHeight;

                    int sampleSize = Math.min(photoW/targetW, photoH/targetH);


                    opt.inJustDecodeBounds = false;
                    opt.inSampleSize = sampleSize;
                    opt.inPurgeable = true;
                    opt.inInputShareable = true;

                    Bitmap bitmap = BitmapFactory.decodeFile(path, opt);
                    Bitmap resized = resize(bitmap, targetW, targetH);

                    ByteArrayOutputStream bao = null;
                    int imageQuality = 100;
                    byte[] ba = null;

                    do {
                        bao = new ByteArrayOutputStream();
                        resized.compress(Bitmap.CompressFormat.JPEG, imageQuality, bao);
                        ba = bao.toByteArray();

                        imageQuality--;
                    } while (ba.length > 100000); // size is less than 100KB


                    ivPhoto.setVisibility(View.VISIBLE);
                    ivPhoto.setImageBitmap(resized);
                    nextbutton.setVisibility(View.INVISIBLE);
                    proceedbutton.setVisibility(View.VISIBLE);



                    imageBinary = bao.toByteArray(); // this image binary to be sent to server

                    String resizedImageFileName = "resized-" + imageFile.getName();
                    resizedImageFile = new File(getPhotoDir(), resizedImageFileName);






                    try {
                        FileOutputStream resizedFileOutputStream = new FileOutputStream(resizedImageFile);
                        try {
                            bao.writeTo(resizedFileOutputStream);
                        } finally {
                            resizedFileOutputStream.close();
                        }
                    } catch (Exception e) {
                        Toast.makeText(this, "Error resizing photo", Toast.LENGTH_LONG).show();

                    }


                    break;
                }
            }
        }
    }


    public static Bitmap resize(Bitmap bitmap, int newWidth, int newHeight) {
        if (bitmap == null) {
            return null;
        }

        int oldWidth = bitmap.getWidth();
        int oldHeight = bitmap.getHeight();

        if (oldWidth < newWidth && oldHeight < newHeight) {
            return bitmap;
        }

        if (oldWidth < oldHeight) { // portrait
            int width = newWidth;
            int height = newHeight;
            newWidth = height;
            newHeight = width;
        }

        float scaleWidth = ((float) newWidth) / oldWidth;
        float scaleHeight = ((float) newHeight) / oldHeight;
        float scaleFactor = Math.min(scaleWidth, scaleHeight);

        Matrix scale = new Matrix();
        scale.postScale(scaleFactor, scaleFactor);

        Bitmap resizeBitmap = Bitmap.createBitmap(bitmap, 0, 0, oldWidth, oldHeight, scale, false);
        //bitmap.recycle();

        return resizeBitmap;
    }





    synchronized public void connect() {

        new AsyncTask<Void, Void, Void>() {
            private void doToast(final String msg) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public Void doInBackground(Void... stuff) {


                try {

                    DatabaseRepository repo = new DatabaseRepository(LoginActivity.this);
//                    uploadStarted();
                    String sitecode =repo.getsitecode();
                    getinfoclient c = new getinfoclient(sitecode);
                    getinfoclient.SnfInfoResult r = c.getInfoTimesheet(loggerSerial, repo.getSnfInfoVersions());
//                    if (r == null) {
//
//                        HeadcheckApplication.setServerState(false);
//                        //doToast("Server Error: Unable to retrieve registers");
//                    } else {
//                        HeadcheckApplication.setServerState(true);
                        if (r.hasNewEmployeesVersion()) {
                            repo.saveEmployees(r.getEmployees());
                            repo.setSnfInfoVersions(r.getSnfInfoVersions());
                        }
//                        devicePsl = r.getDevicePsl();
//                        getAbsenteesData(getBaseContext());
////                        MAX_SHIFT_INTERVAL = calcMaxShift(r.getShiftBlockHours()); //convert to Millis
//                        updateActiveScreen();
                        uploadEnded();
//                    }
                    if (repo.getUnsentEvents().size() >= 0) {
                        uploadStarted();
//                        uploadEvents();
                    }

                } catch (final Exception e) {
                    //doToast("Server Error: " + e.toString());
                    e.printStackTrace();
//                    HeadcheckApplication.setServerState(false);
                }

                return null;
            }

            @Override
            public void onPostExecute(Void result) {

                uploadEnded();
            }

        }.execute();


    }

    synchronized private void uploadEvents() throws Exception {


        DatabaseRepository repo = new DatabaseRepository(LoginActivity.this);

        String sitecode =repo.getsitecode();
        ArrayList<Event> events = repo.getUnsentEvents();
        getinfoclient c = new getinfoclient(sitecode);

        for  (int i = 0; i < events.size() ; i++) {
            JSONObject r = c.logineventupload(events.get(i));
            Integer databaseId = r.getInt("eventid");




            repo.addlogineventid(databaseId, events.get(i).getId());
            employeephoto unsentimage = repo.getphoto(events.get(i).getId());

            Boolean imageauthentication = c.uploadIncidentPhoto(c, unsentimage,databaseId);

            if(currenteventid==events.get(i).getId()){
                if(!imageauthentication){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(LoginActivity.this, "Invalid photo, please try again", Toast.LENGTH_LONG).show();
                        }
                    });
                }else{
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(LoginActivity.this, "Done!", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }

        }

    }


    public void uploadEnded() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                refreshProgressBar = (ProgressBar) topBar.findViewById(R.id.refresh);
//                refreshProgressBar.setVisibility(View.GONE);
            }
        });
    }
    public void uploadStarted() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                topBar = findViewById(R.id.top_bar);
//                refreshProgressBar = (ProgressBar) topBar.findViewById(R.id.refresh);
//                refreshProgressBar.setVisibility(View.VISIBLE);
            }
        });
    }



        public Employees login(String username, String password) {

            DatabaseRepository repo = new DatabaseRepository(this.getApplicationContext());
            Employees user = repo.getEmployeeByUsername(username);


            if (user != null) {

                boolean authenticated = false;
                if (user.getSerialNumber() != null && Util.checkPin(password, user.getSerialNumber())) {
                    authenticated = true;

                    if (!authenticated) {
                        return null;
                    }


                    return user;
                } else {
                    return null;
                }
            }

            return null;
        }




















    synchronized public void upload() {

        new AsyncTask<Void, Void, Void>() {
            private void doToast(final String msg) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public Void doInBackground(Void... stuff) {


                try {

                    DatabaseRepository repo = new DatabaseRepository(LoginActivity.this);

                    if (repo.getUnsentEvents().size() >= 0) {
//                        uploadStarted();
                        uploadEvents();
                    }


                } catch (final Exception e) {
                    //doToast("Server Error: " + e.toString());
                    e.printStackTrace();

//                    HeadcheckApplication.setServerState(false);
                }

                return null;
            }

            @Override
            public void onPostExecute(Void result) {

                uploadEnded();
            }

        }.execute();


    }


    @Override
    protected void onPause() {
        super.onPause();

       handler2.removeCallbacks(runnableCode);


    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler2.removeCallbacks(runnableCode);


    }
    @Override
    protected void onResume() {
        super.onResume();
        handler2.post(runnableCode);


    }

    private void onClickCheckForUpdates() {

        new AsyncTask<Void, Void, UpdateClient.UpdateResult>() {
            String updateUrl = getString(R.string.update_url);
            Exception exception = null;
            private boolean updateAvailable = false;
            @Override
            protected UpdateClient.UpdateResult doInBackground(Void...params) {
                try {
                    return UpdateClient.checkForUpdate(updateUrl);
                } catch (Exception e) {
                    exception = e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(UpdateClient.UpdateResult updateResult) {

                if (updateResult != null) {
                    PackageInfo packageInfo = null;

                    try {
                        packageInfo = UpdateClient.getPackageInfo(LoginActivity.this);
                    } catch (PackageManager.NameNotFoundException e) {
                        Toast.makeText(LoginActivity.this, "Package manager error", Toast.LENGTH_LONG).show();

                        return;
                    }

                    updateAvailable = !updateResult.version.equals(packageInfo.versionName);
                    onClickDownloadAndInstall();
                    if (!updateAvailable) {
                    }
                } else {
                    if (exception != null) {
                        if (exception instanceof UpdateClientException) {
                            Toast.makeText(LoginActivity.this, "Error checking for updates: " + exception.getMessage(), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(LoginActivity.this, "Exception checking for updates", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, "Error checking for updates", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }.execute();
    }


    private void onClickDownloadAndInstall() {
        final UpdateClient.ProgressListener progressListener = new UpdateClient.ProgressListener() {
            @Override
            public void onProgressUpdate(int percent) {

            }
        };
        final String updateUrl = getString(R.string.update_url);
        new AsyncTask<Void, Void, Exception>() {
            @Override
            protected Exception doInBackground(Void...params) {
                try {
                    UpdateClient.doUpdate(LoginActivity.this, updateUrl, progressListener);
                } catch (UpdateClientException e) {
                    return e;
                } catch (Exception e) {
                    return e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Exception e) {
                if (e != null) {
                    if (e instanceof UpdateClientException) {
                        Toast.makeText(LoginActivity.this, "Update Error: " + e.getMessage(), Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(LoginActivity.this, "Update Error", Toast.LENGTH_LONG).show();

                    }
                }
            }
        }.execute();
    }
}

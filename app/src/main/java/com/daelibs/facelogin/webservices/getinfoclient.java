package com.daelibs.facelogin.webservices;

import android.util.Base64;
import android.util.Log;

import com.daelibs.facelogin.data.employeephoto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

public class getinfoclient extends AbstractSnfRpcClient {

    private static String LOGTAG = "SnfInfoClient";



    public getinfoclient(String serviceAddress) {
        super(LOGTAG, serviceAddress);
        setEncryptBody(true);
    }


    public static class Event {
        private long id;
        private Date eventTime;
        private int employeeid;
        private int loginevent;
        private int clockInOut;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public Date getEventTime() {
            return eventTime;
        }

        public void setEventTime(Date eventTime) {
            this.eventTime = eventTime;
        }

        public int getEmployeeId() {
            return employeeid;
        }

        public void setEmployeeId(int employeeid) {
            this.employeeid = employeeid;
        }

        public int getLoginEvent() {
            return loginevent;
        }

        public void setLoginEvent(int loginevent) {
            this.loginevent = loginevent;
        }

        public int getClockInOut() {
            return clockInOut;
        }

        public void setClockInOut(int clockInOut) {
            this.clockInOut = clockInOut;
        }
    }





    public static class Employee {
        private long serverId;
        private String firstName;
        private String lastName;
        private String serialNumber;
        private int tokenType;
        private String empCode;
        private String psl;
        private String username;

        public long getServerId() {
            return serverId;
        }

        public void setServerId(long serverId) {
            this.serverId = serverId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getSerialNumber() {
            return serialNumber;
        }

        public void setSerialNumber(String serialNumber) {
            this.serialNumber = serialNumber;
        }

        public int getTokenType() {
            return tokenType;
        }

        public void setTokenType(int tokenType) {
            this.tokenType = tokenType;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmpCode() {
            return empCode;
        }

        public void setEmpCode(String empCode) {
            this.empCode = empCode;
        }
        public String getUserName() {
            return username;
        }

        public void setUserName(String username) {
            this.username = username;
        }
        public String getPsl() {
            return psl;
        }

        public void setPsl(String psl) {
            this.psl = psl;
        }
    }

    public static class SnfInfoVersions {

        private String employeesVersion;

        public void setEmployeesVersion(String version) {
            this.employeesVersion = version;
        }

        public String getEmployeesVersion() {
            return employeesVersion;
        }
    }

    public static class SnfInfoResult {

        private String databaseId;
        private Collection<Employee> employees;
        private SnfInfoVersions infoVersions;
        private String devicePsl;
        private int shiftBlockHours = 0;

        SnfInfoResult(String databaseId, SnfInfoVersions infoVersions, Collection<Employee> employees, String devicePsl, int shiftBlockHours) {
            this.databaseId = databaseId;
            this.infoVersions = infoVersions;
            this.employees = employees;
            this.devicePsl = devicePsl;
            this.shiftBlockHours = shiftBlockHours;
        }



        public void setShiftBlockHours(int shiftBlockHours) {
            this.shiftBlockHours = shiftBlockHours;
        }

        public String getDatabaseId() {
            return databaseId;
        }

        public Collection<Employee> getEmployees() {
            return employees;
        }

        public SnfInfoVersions getSnfInfoVersions() {
            return infoVersions;
        }

        public boolean hasNewEmployeesVersion() {
            return employees != null;
        }

        public String getDevicePsl() {
            return devicePsl;
        }
    }


    public SnfInfoResult getInfoTimesheet(String serialNumber, SnfInfoVersions currentVersions) {

        try {
            JSONObject params = new JSONObject();
            params.put("serialNumber", serialNumber);
            params.put("emVer", currentVersions.getEmployeesVersion());

            JSONObject result = doRequest("getInfoTimesheet", params);

            String databaseId = result.getString("dbId");

            SnfInfoVersions infoVersions = new SnfInfoVersions();

            infoVersions.setEmployeesVersion(result.getString("emVer"));

            ArrayList<Employee> employees = null;

            String devicePsl = null;

            int shiftBlockHours = 0;


            if (!result.isNull("em")) {
                JSONArray jsonVisitPoints = result.getJSONArray("em");
                employees = new ArrayList<Employee>();

                for (int i = 0; i < jsonVisitPoints.length(); i++) {
                    JSONArray jsonvp = jsonVisitPoints.getJSONArray(i);
                    Employee em = new Employee();
                    em.setTokenType(jsonvp.getInt(0));
                    em.setSerialNumber(jsonvp.getString(1));
                    em.setServerId(jsonvp.getLong(2));
                    em.setFirstName(jsonvp.getString(3));
                    em.setLastName(jsonvp.getString(4));
                    em.setEmpCode(jsonvp.getString(5));
                    em.setUserName(jsonvp.getString(6));
                    em.setPsl(jsonvp.getString(7));

                    employees.add(em);
                }
            }

            if (!result.isNull("psl")) {
                devicePsl = result.getString("psl");
            }

            if (!result.isNull("sbh")) {
                shiftBlockHours = result.getInt("sbh");
            }

            return new SnfInfoResult(databaseId, infoVersions, employees, devicePsl, shiftBlockHours);

        } catch (Exception e) {

            return null;
        }
    }




    public boolean uploadIncidentPhoto(getinfoclient getinfoclient, employeephoto employeePhoto, int eventid)
            throws JSONException, IOException, AbstractSnfRpcClient.SnfRpcException {

        Boolean resultboolean = false;
        byte[] photoBytes = Util.fileToByteArray(employeePhoto.getFileName());

        int employee_id = (int)employeePhoto.getEmployeeId();

        int pos = 0;

        final int CHUNK_SIZE = 20 * 1024;
        int len = photoBytes.length;

        JSONObject result = null;
        result = getinfoclient.uploadfacialrecognitionphotopart(employee_id, eventid,  pos, len, photoBytes);
        if (result != null) {


            if (!result.getBoolean("completed")) {
                resultboolean= false;
            }else{
                File tobedeleted =  new File(employeePhoto.getFileName());
                tobedeleted.delete();
                if (!result.getBoolean("verification")) {
                    resultboolean= false;
                }
                else{
                    resultboolean=true;
                }
            }
        }
         return resultboolean;
    }



    public JSONObject uploadfacialrecognitionphotopart(int employee_id, int eventid,  long startPos, long totalSize, byte[] chunkData)
            throws JSONException, IOException, SnfRpcException {

        String event_id = Integer.toString(eventid);
        String dataString = Base64.encodeToString(chunkData, Base64.NO_WRAP);
        JSONObject body = new JSONObject();
        body.put("employeeid", employee_id);
        body.put("eventid", event_id);
        body.put("startPos", startPos);
        body.put("totalSize", totalSize);
        body.put("photoData", dataString);
        return doRequest("uploadfacialrecognitionphotopart", body);




    }



    public JSONObject logineventupload(com.daelibs.facelogin.data.Event event)
            throws JSONException, IOException, SnfRpcException {
//        String dataString = Base64.encodeToString(chunkData, Base64.NO_WRAP);
        SimpleDateFormat dateFormatLocal = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
          String id = Long.toString(event.getId());
          String employeeid = Integer.toString(event.getEmployeeId());
          String time = dateFormatLocal.format(event.getEventTime());
          String clockinout = Integer.toString(event.getClockInOut());


        JSONObject body = new JSONObject();
            body.put("employeeid", event.getId());
            body.put("employeeid", employeeid);
            body.put("time",time);
            body.put("logintype",    clockinout);
        JSONObject result =  doRequest("logineventupload", body);
        return result;
    }

















}

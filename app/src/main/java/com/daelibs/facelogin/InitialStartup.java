package com.daelibs.facelogin;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.daelibs.facelogin.data.ConfigResult;
import com.daelibs.facelogin.data.Constants;
import com.daelibs.facelogin.data.DatabaseRepository;
import com.daelibs.facelogin.data.DeviceInfo;
import com.daelibs.facelogin.webservices.AbstractSnfRpcClient;
import com.daelibs.facelogin.webservices.RegistrationClient;
import com.daelibs.facelogin.webservices.RemoteSettings;
import com.daelibs.facelogin.webservices.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class InitialStartup extends Activity {
    public WifiManager wifiManager;
    public String url;
    public ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.initialstartup);
        final EditText activationCodeEditText;
        DatabaseRepository repo = new DatabaseRepository(this);
        String sitecode =repo.getsitecode();

        if(sitecode!=null){

            initComplete();
        }

//        if (hintsExist()) {
//            initComplete();
//        }

//
//        startActivityIntent = getIntent().getParcelableExtra(Intent.EXTRA_INTENT);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Activating");

        Button activateButton = (Button) findViewById(R.id.activateButton);

        //loggerSerialTextView = (TextView) findViewById(R.id.loggerSerial);
        //connectionProgressBar = (ProgressBar) findViewById(R.id.connectionProgressBar);
        //statusTextView = (TextView) findViewById(R.id.status);

        activationCodeEditText = (EditText) findViewById(R.id.activation_code);

        //connectionProgressBar.setVisibility(View.GONE);
//
        wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);

        activateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String activationCode = activationCodeEditText.getText().toString().trim();
                if (activationCodeEditText.getText().toString().trim().length() > 0) {
                    doInit();
//                    Toast.makeText(InitialStartup.this, activationCodeEditText.getText().toString(), Toast.LENGTH_LONG).show();


                } else {
                    Toast.makeText(InitialStartup.this, "Please enter an activation code", Toast.LENGTH_LONG).show();
                }
            }
        });

        url = getResources().getString(R.string.registration_service_url);


    }

    private void enableWifi() {
        wifiManager.setWifiEnabled(true);
        for (int i = 0; i < 20 && !wifiManager.isWifiEnabled(); i++) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                return;
            }
        }
    }

    public void initComplete() {
        Util.setDeviceConfigured(this, true);
        Intent newintent = new Intent(InitialStartup.this, LoginActivity.class);
        startActivity(newintent);

        finish();
    }



    private void doInit() {

        progressDialog.show();

        new AsyncTask<Void, String, Void> () {

            String error = null;

            @Override
            public Void doInBackground(Void... args) {

                boolean disableWifi = false;

                if (Util.getSerial(InitialStartup.this) == null) {
                    publishProgress("Attempting to register a serial number");

                    //Make sure wifi is enabled so we can access the Wifi Mac Address
                    if (!wifiManager.isWifiEnabled()) {
                        publishProgress("Enabling Wifi");
                        enableWifi();
                        disableWifi = true;
                    }

                    try {
                        if (!activate()) {
                            error = "Unable to retrieve a serial number";
                            return null;
                        }

                    } catch (AbstractSnfRpcClient.SnfRpcException e) {
                        error = e.getMessage();
                    } catch (JSONException e) {
                        error = "JSON Error";
                    } catch (IOException e) {
                        error = "Network Error " + e.getMessage();
                    }
                    if (disableWifi) {
                        wifiManager.setWifiEnabled(false);
                    }

                    if (error != null) {
                        return null;
                    }
                }


                final String loggerSerialNumber = Util.getSerial(InitialStartup.this);

                publishProgress("Serial number obtained");

                ConfigResult configResult = Util.getRegistrationConfig(InitialStartup.this, url, loggerSerialNumber);

                if (configResult == null) {
                    error = "Unable to retrieve device configuration";
                } else {
//                    DataRepository repo = new DataRepository(getApplicationContext());

                    if (configResult.getInfoUrl() == null) {
                        error = "Please assign this device to a SeeknFind Instance\n\nDevice: " + loggerSerialNumber;
                    } else {
                        Log.e("sitecode", configResult.getInfoUrl());
                        DatabaseRepository repo = new DatabaseRepository(getApplicationContext());
                        repo.setsitecode(configResult.getInfoUrl());
                        initComplete();
                    }

                    if (configResult.getGelatiUploadUrl() != null) {
                        SharedPreferences p = getSharedPreferences(Constants.PREFS_NAME, 0);
                        SharedPreferences.Editor e = p.edit();
                        e.putString(Constants.PREFS_GELATI_UPLOAD_URL, configResult.getGelatiUploadUrl());
                        e.commit();
                    }

                    try {
                        RegistrationClient s = new RegistrationClient(url);
                        JSONObject settingsJson = s.getSettings(loggerSerialNumber);
                        RemoteSettings remoteSettings = new RemoteSettings(InitialStartup.this);
                        remoteSettings.applyJson(settingsJson);
                    } catch (JSONException e) {
                        error = "Error parsing settings";
                    } catch (AbstractSnfRpcClient.SnfRpcException e) {
                        error = "RPC Error";
                    } catch (IOException e) {
                        error = "Network Error retrieving settings";
                    }

                }




                return null;
            }

            @Override
            public void onProgressUpdate(String... args) {
                //setStatus(args[0]);
                progressDialog.setMessage(args[0]);
            }

            @Override
            public void onPostExecute(Void result) {
                try {
                    progressDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (error != null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(InitialStartup.this);
                    b.setTitle("Error Activating Device");
                    b.setMessage(error);
                    b.show();
                }

            }

        }.execute();

    }

    private boolean activate () throws JSONException, IOException, AbstractSnfRpcClient.SnfRpcException {
        RegistrationClient s = new RegistrationClient(url);
        RegistrationClient.GetSerialResult result = null;

        String hintSite = null;
        String hintConfiguration = null;
//
//        Hints hints = loadHints();
        EditText activationCodeEditText = (EditText) findViewById(R.id.activation_code);
        String activationCode = activationCodeEditText.getText().toString();
//
//        if (hints != null) {
//            hintSite = hints.site;
//            hintConfiguration = hints.configuration;
//        }

        DeviceInfo deviceInfo = Util.createDeviceConfig(this);

        result = s.activate(deviceInfo, activationCode, hintSite, hintConfiguration);


        if (result != null) {
            Util.setLoggerSerialNumber(this, result.getSerialNumber());
            writeRegFile(result.getSerialNumber());
            return true;
        } else {
            return false;
        }
    }


    private void writeRegFile(String serialNumber) {
        File file = new File(getExternalFilesDir(null), "reg.info");
        FileWriter fw = null;
        try {

            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                Log.e("permission"," not granted");
            } else {
                 Log.e("permission","granted");
            }
            String imei = telephonyManager.getDeviceId();
            if (imei == null) {
                imei = "";
            }

            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            String wifiMac = "";
            if (wifiInfo != null) {
                wifiMac = wifiInfo.getMacAddress();
                if (wifiMac == null) {
                    wifiMac = "";
                }
            }

            fw = new FileWriter(file.getCanonicalPath());
            fw.write(serialNumber + ",");
            fw.write(imei + ",");
            fw.write(wifiMac + "\r\n");
        } catch (Exception ex) {
            Toast.makeText(this, "Unable to write reg file", Toast.LENGTH_LONG).show();
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException ex) {
                    //
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
//    private static class Hints {
//        public String site;
//        public String configuration;
//    }

//    private boolean hintsExist() {
//        return getHintsFile().exists();
//    }
//
//    private File getHintsFile() {
//        File storageDirectory = Environment.getExternalStorageDirectory();
//        File hintsFile = new File(storageDirectory, "seeknfind.hints");
//        return hintsFile;
//    }
//
//    private Hints loadHints() {
//
//        File hintsFile = getHintsFile();
//
//        FileReader f = null;
//
//        try {
//            f = new FileReader(hintsFile);
//            BufferedReader reader = new BufferedReader(f);
//            String line1 = reader.readLine();
//            String line2 = reader.readLine();
//            Hints hints = new Hints();
//            hints.site = line1;
//            hints.configuration = line2;
//            return hints;
//        } catch (IOException e) {
//            Log.e("unable to load", e.toString());
//        } finally {
//            if (f != null) {
//                try {
//                    f.close();
//                } catch (IOException e) {
//                    //pass
//                }
//            }
//        }
//
//        return null;
//    }
//




}

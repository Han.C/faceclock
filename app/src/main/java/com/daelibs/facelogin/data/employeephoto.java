package com.daelibs.facelogin.data;

public class employeephoto {

        private long id;
        private long eventId;
        private int employeeId;
        private String fileName;
        private int sent;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public int getEmployeeId() {
            return employeeId;
        }

        public void setEmployeeId(int employeeId) {
            this.employeeId = employeeId;
        }

        public long getEventId() {
            return eventId;
        }

        public void setEventId(long eventId) {
            this.eventId = eventId;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public int isSent() {
            return sent;
        }

        public void setSent(int sent) {
            this.sent = sent;
        }



}


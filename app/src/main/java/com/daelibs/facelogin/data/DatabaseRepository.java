package com.daelibs.facelogin.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.daelibs.facelogin.webservices.getinfoclient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

public class DatabaseRepository {
    private SimpleDateFormat dateFormatLocal = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);


    DatabaseHelper openHelper;
    SQLiteDatabase db;

    private static final String[] EVENT_COLS = new String[]{
            DatabaseHelper.COL_EVENT_ID,
            DatabaseHelper.COL_EVENT_EVENT_TIME,
            DatabaseHelper.COL_EVENT_EMPLOYEE_ID,
            DatabaseHelper.COL_EVENT_LOGIN_EVENT,
            DatabaseHelper.COL_EVENT_CLOCK_IN_OUT,
    };

    private static final String[] EMPLOYEE_COLS = new String[]{
            DatabaseHelper.COL_EMPLOYEE_ID,
            DatabaseHelper.COL_EMPLOYEE_SERVER_ID,
            DatabaseHelper.COL_EMPLOYEE_SERIAL_NUMBER,
            DatabaseHelper.COL_EMPLOYEE_TOKEN_TYPE,
            DatabaseHelper.COL_EMPLOYEE_FIRST_NAME,
            DatabaseHelper.COL_EMPLOYEE_LAST_NAME,
            DatabaseHelper.COL_EMPLOYEE_EMP_CODE,
            DatabaseHelper.COL_EMPLOYEE_PSL,
    };


    public DatabaseRepository(Context context) {
        openHelper = new DatabaseHelper(context);
        db = openHelper.getWritableDatabase();
    }


    public void setsitecode(String sitecode) {
        String qry4 = "SELECT * from " + DatabaseHelper.TABLE_CONFIG;

        Integer activeprecinct;
        Cursor c4 = db.rawQuery(qry4, null);
        try {
            if (c4.moveToFirst()) {

                ContentValues cv = new ContentValues();
                cv.put(DatabaseHelper.CONFIG_COL_SITE_ID, sitecode);
                db.update(DatabaseHelper.TABLE_CONFIG, cv, DatabaseHelper.CONFIG_COL_ID + " = 1", null);
            } else {
                ContentValues cv = new ContentValues();
                cv.put(DatabaseHelper.CONFIG_COL_SITE_ID, sitecode);
                db.insert(DatabaseHelper.TABLE_CONFIG, null, cv);
            }

        }finally {
        c4.close();
        }
    }


    public String getsitecode() {
        String qry = "SELECT * from " + DatabaseHelper.TABLE_CONFIG + " WHERE " + DatabaseHelper.CONFIG_COL_ID + " = 1";

        String sitecode;
        Cursor c = db.rawQuery(qry, null);
        try{
            if (c.moveToFirst()) {
                sitecode =c.getString(1);
            }else{
                sitecode = null;
            }

        }finally {
            c.close();
        }
        return sitecode;
    }


    public void insertEmployee(Employees employee) {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COL_EMPLOYEE_FIRST_NAME, employee.getFirstName());
        cv.put(DatabaseHelper.COL_EMPLOYEE_LAST_NAME, employee.getLastName());
        cv.put(DatabaseHelper.COL_EMPLOYEE_SERIAL_NUMBER, employee.getSerialNumber());
        cv.put(DatabaseHelper.COL_EMPLOYEE_TOKEN_TYPE, employee.getTokenType());
        cv.put(DatabaseHelper.COL_EMPLOYEE_SERVER_ID, employee.getServerId());
        cv.put(DatabaseHelper.COL_EMPLOYEE_EMP_CODE, employee.getEmpCode());
        cv.put(DatabaseHelper.COL_EMPLOYEE_USERNAME, employee.getUsername());
        cv.put(DatabaseHelper.COL_EMPLOYEE_PSL, employee.getPsl());



        long id = db.insert(DatabaseHelper.TABLE_EMPLOYEE, null, cv);
        employee.setId(id);
    }
    public long insertevents(Event loginevent) {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COL_EVENT_EVENT_TIME,  dateFormatLocal.format(loginevent.getEventTime()));
        cv.put(DatabaseHelper.COL_EVENT_EMPLOYEE_ID, loginevent.getEmployeeId());
        cv.putNull(DatabaseHelper.COL_EVENT_LOGIN_EVENT);
        cv.put(DatabaseHelper.COL_EVENT_CLOCK_IN_OUT, loginevent.getClockInOut());


        long id = db.insert(DatabaseHelper.TABLE_EVENT, null, cv);

        return id;
    }

    public void saveEmployees(Collection<getinfoclient.Employee> newEmployees) {
        db.delete(DatabaseHelper.TABLE_EMPLOYEE, null, null);

        for (getinfoclient.Employee src : newEmployees) {
            Employees dst = new Employees();
            dst.setTokenType(src.getTokenType());
            dst.setSerialNumber(src.getSerialNumber());
            dst.setServerId(src.getServerId());
            dst.setFirstName(src.getFirstName());
            dst.setLastName(src.getLastName());
            dst.setEmpCode(src.getEmpCode());
            dst.setUsername(src.getUsername());
            dst.setPsl(src.getPsl());
            insertEmployee(dst);

        }
    }


    private void updateDbInfo(String key, String value) {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COL_DBINFO_VALUE, value);
        db.update(DatabaseHelper.TABLE_DBINFO, cv, DatabaseHelper.COL_DBINFO_KEY + " = ?", new String[]{key});
    }

    public void setSnfInfoVersions(getinfoclient.SnfInfoVersions versions) {
        updateDbInfo(DatabaseHelper.DBINFO_KEY_EMPLOYEES_VERSION, versions.getEmployeesVersion());
    }


    private String getDbInfoValue(String key) {
        Cursor c = db.query(
                DatabaseHelper.TABLE_DBINFO,
                new String[]{DatabaseHelper.COL_DBINFO_VALUE},
                DatabaseHelper.COL_DBINFO_KEY + " = ?",
                new String[]{key},
                null, null, null
        );

        String s = null;

        if (c.moveToFirst()) {
            s = c.getString(0);
        }
        return s;
    }

    public getinfoclient.SnfInfoVersions getSnfInfoVersions() {
        getinfoclient.SnfInfoVersions result = new getinfoclient.SnfInfoVersions();
        result.setEmployeesVersion(getDbInfoValue(DatabaseHelper.DBINFO_KEY_EMPLOYEES_VERSION));
        return result;
    }




    public Employees getEmployeeByUsername(String name1) {

        Cursor c = db.query(DatabaseHelper.TABLE_EMPLOYEE,
                EMPLOYEE_COLS,
                DatabaseHelper.COL_EMPLOYEE_USERNAME + " = ?",
                new String[]{name1},
                null, null, null);



            try {
                if (c.moveToFirst()) {
                    Employees emp = new Employees();
                    emp.setId(c.getInt(0));
                    emp.setServerId(c.getInt(1));
                    emp.setSerialNumber(c.getString(2));
                    emp.setTokenType(c.getInt(3));
                    emp.setFirstName(c.getString(4));
                    emp.setLastName(c.getString(5));
                    emp.setEmpCode(c.getString(6));
                    emp.setPsl(c.getString(7));
                    emp.setUsername(name1);


                    return emp;
                } else {
                    return null;
                }
            } finally {
                c.close();
            }
        }

    public void getcurrentstatefromemployee(getinfoclient.SnfInfoVersions versions) {
        updateDbInfo(DatabaseHelper.DBINFO_KEY_EMPLOYEES_VERSION, versions.getEmployeesVersion());
    }



    private static int POS_EVENT_ID = 0;
    private static int POS_EVENT_EVENT_TIME = 1;
    private static int POS_EVENT_TYPE_ID = 2;
    private static int POS_EVENT_SERIAL_NUMBER = 3;
    private static int POS_EVENT_CLOCK_IN_OUT = 4;

    public Event mapEventFromCursor(Cursor c) throws ParseException {
        Event event = new Event();
        event.setId(c.getLong(0));
        event.setEventTime(dateFormatLocal.parse(c.getString(1)));
        event.setEmployeeId(c.getInt(2));
        event.setLoginEvent(c.getInt(3));
        event.setClockInOut(c.getInt(4));













        return event;
    }



    public ArrayList<Event> getUnsentEvents() throws ParseException {

//        long lastSent = getEventSyncId();


        Cursor c = db.rawQuery("SELECT * FROM " + DatabaseHelper.TABLE_EVENT + " WHERE " + DatabaseHelper.COL_EVENT_LOGIN_EVENT + " IS NULL", null);

        ArrayList<Event> l = new ArrayList<Event>();

        try {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                l.add(mapEventFromCursor(c));
                c.moveToNext();
            }
        } finally {
            c.close();
        }


        return l;
    }
    public long getEventSyncId() {
        String v = getDbInfoValue(DatabaseHelper.DBINFO_KEY_EVENT_SYNC);
        return Long.parseLong(v);
    }





    public void insertEmployeePhoto(employeephoto employeephoto) {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.EMPLOYEE_PHOTO_COL_EVENT_ID, employeephoto.getEventId());
        cv.put(DatabaseHelper.EMPLOYEE_PHOTO_COL_EMPLOYEE_ID, employeephoto.getEmployeeId());
        cv.put(DatabaseHelper.EMPLOYEE_PHOTO_COL_FILENAME, employeephoto.getFileName());
        cv.put(DatabaseHelper.EMPLOYEE_PHOTO_COL_SENT, 0);
        long id = db.insert(DatabaseHelper.TABLE_EMPLOYEE_PHOTO, null, cv);
//        employeephoto.setId(id);
    }



    public void addlogineventid(int server_id, long eventid) {

                ContentValues cv = new ContentValues();
                cv.put(DatabaseHelper.COL_EVENT_LOGIN_EVENT, server_id);
                db.update(DatabaseHelper.TABLE_EVENT, cv, DatabaseHelper.COL_EVENT_ID + " = " + eventid, null);

    }


    public employeephoto getphoto(long id ) {
        String qry = "SELECT * from " + DatabaseHelper.TABLE_EMPLOYEE_PHOTO + " WHERE " + DatabaseHelper.EMPLOYEE_PHOTO_COL_EVENT_ID + " = " + id;
        employeephoto employeePhoto;
        Cursor c = db.rawQuery(qry, null);
        try{
            if (c.moveToFirst()) {


                employeephoto employeePhoto1 = new employeephoto();
                employeePhoto1.setId(c.getInt(0));
                employeePhoto1.setEventId(c.getInt(1));
                employeePhoto1.setEmployeeId(c.getInt(2));
                employeePhoto1.setFileName(c.getString(3));
                employeePhoto1.setSent(c.getInt(4));
                employeePhoto = employeePhoto1;

            }else{
                employeePhoto = null;
            }

        }finally {
            c.close();
        }
        return employeePhoto;
    }




}

package com.daelibs.facelogin.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class DatabaseHelper extends SQLiteOpenHelper {



    public static final String DATABASE_NAME = "Facelogindata";

    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_CONFIG = "site_config";

    public static final String CONFIG_COL_ID = "id";
    public static final String CONFIG_COL_SITE_ID = "siteid";

    public static final String TABLE_EMPLOYEE = "employee";
    public static final String TABLE_EVENT = "event";
    public static final String TABLE_DBINFO = "dbinfo";



    public static final String COL_EMPLOYEE_ID = "id";
    public static final String COL_EMPLOYEE_SERVER_ID = "server_id";
    public static final String COL_EMPLOYEE_SERIAL_NUMBER = "serial_number";
    public static final String COL_EMPLOYEE_TOKEN_TYPE = "token_type";
    public static final String COL_EMPLOYEE_FIRST_NAME = "first_name";
    public static final String COL_EMPLOYEE_LAST_NAME = "last_name";
    public static final String COL_EMPLOYEE_EMP_CODE = "emp_code";
    public static final String COL_EMPLOYEE_PSL = "psl";
    public static final String COL_EMPLOYEE_USERNAME = "username";



    public static final String TABLE_EMPLOYEE_PHOTO = "employeephoto";

    public static final String EMPLOYEE_PHOTO_COL_ID = "id";
    public static final String EMPLOYEE_PHOTO_COL_EVENT_ID = "incidentid";
    public static final String EMPLOYEE_PHOTO_COL_EMPLOYEE_ID = "number";
    public static final String EMPLOYEE_PHOTO_COL_FILENAME = "filename";
    public static final String EMPLOYEE_PHOTO_COL_SENT = "sent";





    public static final String COL_EVENT_ID = "id";
    public static final String COL_EVENT_EVENT_TIME = "event_time";
    public static final String COL_EVENT_EMPLOYEE_ID = "employeeid";
    public static final String COL_EVENT_LOGIN_EVENT = "login_event";
    public static final String COL_EVENT_CLOCK_IN_OUT = "clock_in_out";






    public static final String COL_DBINFO_KEY = "k";
    public static final String COL_DBINFO_VALUE = "v";

    public static final String DBINFO_KEY_EVENT_SYNC = "event_sync_id";
    public static final String DBINFO_KEY_EMPLOYEES_VERSION = "employees_version";





    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    private String createIndexSql(String tableName, String columnName) {
        return "CREATE INDEX " + tableName + "_" + columnName +"_idx " +
                "on " + tableName + "(" + columnName + ");";
    }








    @Override
    public void onCreate(SQLiteDatabase db) {


        db.execSQL("CREATE TABLE " + TABLE_CONFIG + " (" +
                CONFIG_COL_ID + " integer primary key autoincrement, "+
                CONFIG_COL_SITE_ID + " text " +
                ");");

        db.execSQL(createIndexSql(TABLE_CONFIG, CONFIG_COL_ID));
        db.execSQL(createIndexSql(TABLE_CONFIG, CONFIG_COL_SITE_ID));



        db.execSQL("CREATE TABLE " + TABLE_EMPLOYEE + " ( " +
                COL_EMPLOYEE_ID + " integer primary key autoincrement, " +
                COL_EMPLOYEE_SERVER_ID + " integer, " +
                COL_EMPLOYEE_SERIAL_NUMBER + " text," +
                COL_EMPLOYEE_TOKEN_TYPE + " integer, " +
                COL_EMPLOYEE_FIRST_NAME + " text, " +
                COL_EMPLOYEE_LAST_NAME + " text, " +
                COL_EMPLOYEE_EMP_CODE + " text, " +
                COL_EMPLOYEE_USERNAME + " text, " +
                COL_EMPLOYEE_PSL + " text " +
                ");");





        db.execSQL(createIndexSql(TABLE_EMPLOYEE, COL_EMPLOYEE_USERNAME));







        db.execSQL("CREATE TABLE " + TABLE_EVENT + " (" +
                COL_EVENT_ID + " integer primary key autoincrement," +
                COL_EVENT_EVENT_TIME + " text," +
                COL_EVENT_EMPLOYEE_ID + " integer," +
                COL_EVENT_LOGIN_EVENT + " integer, " +
                COL_EVENT_CLOCK_IN_OUT + " integer" +
                ")");

        db.execSQL("CREATE TABLE " + TABLE_DBINFO + " (" +
                COL_DBINFO_KEY + " text unique," +
                COL_DBINFO_VALUE + " text " +
                ")");

        db.execSQL("INSERT INTO " + TABLE_DBINFO + " (k, v) VALUES ('" + DBINFO_KEY_EVENT_SYNC + "', 0);");
        db.execSQL("INSERT INTO " + TABLE_DBINFO + " (k, v) VALUES ('" + DBINFO_KEY_EMPLOYEES_VERSION + "', '0');");






        db.execSQL("CREATE TABLE " + TABLE_EMPLOYEE_PHOTO + "( " +
                EMPLOYEE_PHOTO_COL_ID +  " integer primary key autoincrement, " +
                EMPLOYEE_PHOTO_COL_EVENT_ID + " integer, " +
                EMPLOYEE_PHOTO_COL_EMPLOYEE_ID + " integer, " +
                EMPLOYEE_PHOTO_COL_FILENAME + " text, " +
                EMPLOYEE_PHOTO_COL_SENT + " integer " +
                ");");















    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int currentVersion, int latestVersion) {
        if (currentVersion < latestVersion) {
            //run all the required applyVersionN methods:
            for (int v = currentVersion + 1; v <= latestVersion; v++) {
                String methodName = "applyVersion" + v;
                Method applyMethod = null;
                try {
                    applyMethod = this.getClass().getMethod(methodName, SQLiteDatabase.class);
                } catch (NoSuchMethodException e) {
                    throw new RuntimeException("DatabaseUpgrade " + methodName + " method not found", e);
                }

                try {
                    applyMethod.invoke(this, db);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException("invoking methodName", e);
                } catch (InvocationTargetException e) {
                    throw new RuntimeException("invoking methodName", e);
                }
            }
        }
    }
}

package com.daelibs.facelogin.data;
import java.util.Date;

/**
 * Created by ryan on 1/07/2014.
 */
public class Event{
    private long id;
    private Date eventTime;
    private int employeeid;
    private int loginevent;
    private int clockInOut;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getEventTime() {
        return eventTime;
    }

    public void setEventTime(Date eventTime) {
        this.eventTime = eventTime;
    }

    public int getEmployeeId() {
        return employeeid;
    }

    public void setEmployeeId(int employeeid) {
        this.employeeid = employeeid;
    }

    public int getLoginEvent() {
        return loginevent;
    }

    public void setLoginEvent(int loginevent) {
        this.loginevent = loginevent;
    }

    public int getClockInOut() {
        return clockInOut;
    }

    public void setClockInOut(int clockInOut) {
        this.clockInOut = clockInOut;
    }


//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        Event event = (Event) o;
//
//        if (id != event.id) return false;
//        return serialNumber.equals(event.serialNumber);
//
//    }
//
//    @Override
//    public int hashCode() {
//        int result = (int) (id ^ (id >>> 32));
//        result = 31 * result + serialNumber.hashCode();
//        return result;
//    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        Event event = (Event) o;
//
//        return serialNumber.equals(event.serialNumber);
//
//    }
//
//    @Override
//    public int hashCode() {
//        return serialNumber.hashCode();
//    }
//
//    @Override
//    public int compareTo(Event another) {
//        return this.getEventTime().compareTo(another.getEventTime());
//    }

}
